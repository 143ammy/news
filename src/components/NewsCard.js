import React from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Skeleton from "@mui/material/Skeleton";
import Box from "@mui/material/Box";
function NewsCard(props) {
  return (
    <div>
      <Card style={{ margin: "30px", width: "400px", height: "500px" }}>
        {!props.loading ? (
          <div>
            <img
              src={props.imageUrl}
              style={{
                width: "400px",
                height: "200px",
                objectFit: "cover",
              }}
            />
            <CardContent>
              <Typography
                sx={{ fontSize: 18, fontWeight: "600", fontFamily: "inherit" }}
                gutterBottom
              >
                {props.title.length > 100
                  ? props.title?.substring(0, 100) + "..."
                  : props.title}
              </Typography>
              <Typography
                sx={{
                  color: "gray",
                  fontStyle: "italic",
                  fontFamily: "cursive",
                }}
              >
                {props?.source?.name}
              </Typography>

              <Typography variant="body2">
                {props?.description?.length > 150
                  ? props.description?.substring(0, 150) + "..."
                  : props.description}
              </Typography>
            </CardContent>
            <CardActions>
              <Button size="small">
                <a
                  style={{
                    textDecoration: "none",
                    color: "red",
                    fontWeight: "600",
                  }}
                  href={props.link}
                >
                  Read More
                </a>
              </Button>
            </CardActions>{" "}
          </div>
        ) : (
          <Skeleton variant="rectangular" width="100%" height="80%">
            <div style={{ paddingTop: "60%" }} />
          </Skeleton>
        )}
      </Card>
    </div>
  );
}

export default NewsCard;
