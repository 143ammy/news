import React, { useEffect, useState } from "react";

import NewsCard from "./components/NewsCard";
import Navbar from "./components/Navbar";
import { Button } from "@mui/material";
import Autocomplete from "@mui/material/Autocomplete";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import LinearProgress from "@mui/material/LinearProgress";

function News() {
  const [articles, setArticals] = useState([]);
  const [country, setCountry] = useState("in");
  const [category, setCategory] = useState("general");
  const [page, setPage] = useState(1);
  const [Loading, setLoading] = useState(false);
  const [searchQuery, setSearchQuery] = useState("");
  const [total, setTotal] = useState(0);
  const categoryList = [
    "business",
    "entertainment",
    "general",
    "health",
    "science",
    "sports",
    "technology",
  ];

  useEffect(() => {
    setLoading(true);
    let apiUrl = `https://newsapi.org/v2/top-headlines?country=${country}&category=${category}&apiKey=902963a7b1be4873902d6b018259de55&page=${page}`;

    if (searchQuery)
      apiUrl = `https://newsapi.org/v2/top-headlines?q=${searchQuery}&apiKey=902963a7b1be4873902d6b018259de55`;

    fetch(apiUrl)
      .then((response) => response.json())
      .then((data) => {
        setArticals(data?.articles || []);
        setTotal(data?.totalResults);
        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
        setLoading(false);
      });
  }, [page, category, country, searchQuery]);

  return (
    <div style={{ width: "100%", height: "100vh" }}>
      <Navbar
        category={categoryList}
        selectedCategory={category}
        setCategory={setCategory}
        setCountry={setCountry}
        country={["in", "us"]}
        setSearchQuery={setSearchQuery}
        selectedCountry={country}
      />
      {Loading ? (
        <Box sx={{ width: "100%", marginTop: "65px" }}>
          {" "}
          <LinearProgress style={{ height: "5px" }} />
        </Box>
      ) : null}
      <div
        style={{ display: "flex", flexDirection: "column", marginTop: "80px" }}
      >
        <div
          style={{
            display: "flex",
            flexWrap: "wrap",
            justifyContent: "center",
          }}
        >
          {articles && articles?.length > 0 ? (
            articles.map((item) => (
              <NewsCard
                title={item?.title}
                source={item?.source}
                description={item?.description}
                imageUrl={item?.urlToImage}
                link={item?.url}
                loading={Loading}
              />
            ))
          ) : (
            <div>
              <p>Unable to Fectch Articles</p>
            </div>
          )}
        </div>
        {articles.length > 0 && (
          <div
            style={{
              height: "50px",
              textAlign: "center",
              display: "flex",
              justifyContent: "space-between",
              margin: "auto",
            }}
          >
            {page > 1 && (
              <Button
                color="primary"
                size="medium"
                onClick={() => setPage(page - 1)}
              >
                Previous
              </Button>
            )}
            {page <= total / 20 && (
              <Button
                color="primary"
                size="medium"
                onClick={() => setPage(page + 1)}
              >
                Next
              </Button>
            )}
          </div>
        )}
      </div>
    </div>
  );
}

export default News;
